## Latest stable release tag

![Stable](https://gitlab.com/whitespots-public/auditor/-/badges/release.svg?order_by=release_at&value_width=150)


# Deployment process

* **Install**:
  * Option 1: Using containers:
    * [Deploy using GitLab CI (Ansible playbook)](/README.md#install-using-ansible-playbook)
    * [Manual Installation](/README.md#manual-installation)
  * Option 2: For Kubernetes environment:
    * [Install using Helm](/README.md#install-using-helm)
  
* **Update:**
  * Option 1: Using containers:
    * [Update using GitLab CI (Ansible playbook)](/README.md#update-using-gitlab-ci-ansible-playbook)
    * [Manual update](/README.md#manual-update)
  * Option 2: For Kubernetes environment:
    * [Update using Helm](/README.md#update-in-kubernetes-environment)
   
  

## Installation
## Using containers

### Prerequisites
Before installing the AppSec Portal, make sure you have the following software installed on your machine:
* [**Docker**](https://docs.docker.com/get-docker/) (version 19.03 or higher)
* [**Docker Compose**](https://docs.docker.com/compose/install/) (version 1.26 or higher)
* [**GitLab Runner**](https://docs.gitlab.com/runner/install/) (for GitLab CI installation option)
* **SSH keys**

<details><summary>Expand to see how to generate SSH keys</summary><br/>
To securely connect to the Linux server, you will need to set up SSH keys.\
If you don't have SSH keys already, you can generate them using the following command in your server terminal:</e>

```bash
ssh-keygen -t rsa -b 4096
```

After generating the SSH keys, you need to copy the public SSH key to the Linux server. Use this command to copy the public key:

```bash
ssh-copy-id <username>@<server-ip-address>
```

Replace ***username*** with your Linux server account username, and ***server-ip-address*** with the IP address of the Linux server. You will be prompted to enter your password for authentication.\
Open the file on your local machine where the private SSH key is stored. The private key is typically saved with a .pem or .ssh file extension.\
Select and copy the contents of the private key file.\
Ensure you copy the key with the correct permissions and line breaks intact.</details>

### Install using Ansible playbook
#### Step 1.  Fork the Auditor Repository
Fork the Auditor repository on GitLab. This creates a copy of the repository under your GitLab account.
#### Step 2. Set the public SSH key on the host
Establish a secure connection between the host and the repository by setting the public SSH key.
#### Step 3. Configure GitLab CI/CD Environment Variables
In GitLab, go to _"Settings" > "CI / CD" > "Variables"_ and configure the following environment variables:

* SSH_KEY_PRIVATE: Set the private SSH key within the forked repository for authentication.
* ACCESS_TOKEN: set the Access Token value that you will receive after the first run of CI Pipeline (step 9)

**Optional** environment variables:
* IMAGE_VERSION: The script will autonomously determine the most recent version.
* DB_NAME, DB_USER, DB_PASS, DB_HOST, DB_PORT: Required for database configuration.
* RABBITMQ_DEFAULT_USER, RABBITMQ_DEFAULT_PASS, AMQP_HOST_STRING: Message broker configuration.
#### Step 4. Update the Hosts File
In the repository's **hosts** file, specify the group name and IP address of the hosts where Auditor will be installed: 

`[prod_portal]` - name of the group
<br>`206.189.63.52` - IP address
#### Step 5. Update Variables in prod_portal.yml
Update the variables in the **prod_portal.yml** file in the  
**group_vars** directory
```bash
ansible_user: root 
ansible_ssh_private_key : ~/.ssh/id_rsa
work_dir: /opt
```
`ansible_user`: Specify the user Ansible should use when connecting to the server
<br>`ansible_ssh_private_key`:Specify the path to the private SSH key for authentication
<br>`work_dir`: The working directory on the target server where the application will be installed
#### Step 6. Commit Changes
After updating the hosts file and group_vars/prod_portal.yml, commit the changes to your GitLab repository
#### Step 7. Run GitLab CI Pipeline
In the GitLab CI/CD > Pipelines section, you should see the pipeline running the deploy job.
#### Step 8. Monitor the Installation
Once the pipeline is running, click on the deploy job to view the logs. 
<br>The Ansible playbook will be executed, deploying Auditor on the specified host.

<br>![Image](docs/images/ci_deploy.jpg)

#### Step 9. Adding an Access Token
After the first run, you will receive an Access Token.
> **⚠️ WARNING:** Copy the value of the access token and [add](/README.md#step-3-configure-gitlab-cicd-environment-variables) it in the CI/CD variables on GitLab

### Manual Installation

#### Step 1: Clone the repository

Clone the Auditor repository to your server:

```bash
git clone https://gitlab.com/whitespots-public/auditor.git auditor
```

#### Step 2 Navigate to the root directory

Navigate to the root directory of the Auditor project by executing the following command:

```bash
cd auditor
```

#### Step 3: Set environment variables

Environment variables are set by default.
<br>If changes are needed, create an **.env** file in the project's root folder.
Example .env file:

```bash
DB_NAME=postgres
DB_USER=postgres
DB_PASS=postgres
DB_HOST=postgres
DB_PORT=5432
RABBITMQ_DEFAULT_USER=admin
RABBITMQ_DEFAULT_PASS=mypass
AMQP_HOST_STRING=amqp://admin:mypass@rabbitmq:5672/
DOCKER_ENCRYPTION_TOKEN=defaultvaluetobechangedorelse...
```

- `DB_NAME`, `DB_USER`, `DB_PASS`, `DB_HOST`, `DB_PORT` variables are required for database configuration.
- If the message broker is hosted on a third-party server, only the `AMQP_HOST_STRING` must be specified. However, if the container is raised locally, all three variables, including `RABBITMQ_DEFAULT_USER` and `RABBITMQ_DEFAULT_PASS` need to be specified.
- `DOCKER_ENCRYPTION_TOKEN` this variable is essential when accessing images from a private registry. If your registry requires authentication, provide the appropriate encryption token here.


#### Step 4. Start the Auditor

From the terminal command line, navigate to the directory where the docker-compose.yml file is located.

Run the application by executing the following command:

```bash
docker-compose up -d
```
This will start all the services described in the docker-compose.yml file in the background.
#### Step 5. Verify that your application is running

 After successfully running the docker-compose up -d command, your application should be accessible on port 8080 (HTTP) and port 8443 (HTTPS), if such ports are configured.

## Install in Kubernetes environment
### Install using Helm
Before using Helm, make sure that Helm is installed on your computer and that your Kubernetes cluster is configured to work with Helm

#### Step 1. Clone the repository
Clone the Auditor repository to your server:
```bash
git clone https://gitlab.com/whitespots-public/auditor.git auditor
```

#### Step 2. Navigate to the root directory
Navigate to the directory where the Auditor files were cloned, the helm directory:
```bash
cd auditor/AuditorHelmChart
```

#### Step 3. Set environment variables
in the **values.yaml** file, change the default environment variables to meet your requirements:

* In the **deploymentSpec** section:
```bash
    release: release_v23.11.1
```
* In the **configMap** section:
```bash
  DB_HOST: "postgres"     
  DB_PORT: "5432"         
  DB_NAME: "postgres"       
  DB_USER: "postgres"                
  DOMAIN: http://localhost  
  RABBITMQ_DEFAULT_USER: "admin"
  RABBITMQ_DEFAULT_PORT: "5672"
```

* In the **secrets** section:
```bash
  AMQP_HOST_STRING: "amqp://admin:mypass@rabbitmq:5672/"
  DB_PASS: "postgres"
  RABBITMQ_DEFAULT_PASS: "mypass"
  REDIS_PASSWORD: "11110000"
```

<br>`release` specify a particular release identifier 
<br>`DB_NAME`, `DB_USER`, `DB_HOST`, `DB_PORT` and `DB_PASS` variables are required for database configuration.    
If the message broker is hosted on a third-party server, only the `AMQP_HOST_STRING` must be specified. However, if the container is raised locally, all three variables, including `RABBITMQ_DEFAULT_USER` and `RABBITMQ_DEFAULT_PASS` need to be specified
<br> `REDIS_PASSWORD` If the broker is hosted on a third-party server leave the variable at its default value

* In the **db** section:
<br>It is **recommended** to use an **external database**. For this purpose it is enough only to specify the value `true` for the variable `external_db`, other variables in this section do not need to be specified

But if you use a database inside the cluster, configure variables for it
```bash
external_db: false 
name: postgres
storageClassName: local-storage
node: minikube
path: /mnt/local-storage
mountPath: /mnt
claimName: postgres-pv-claim
```
`external_db`: `false` 
<br>`name`: database name
<br>`storageClassName`: storage class name for the database
<br>`node`: the node in the cluster that will host the database
<br>`path`:path to the database storage on the node
<br>`mountPath`: the place inside the container where the database storage will be mounted
<br>`claimName`: the name of the PersistentVolumeClaim that is used to request storage allocation

#### Step 4. Install the application using Helm
Run the application by executing the following command:
```bash
helm install auditor <path-to-helm-directory>
```
replace *<path-to-helm-directory>* with the path to the directory that contains the Helm Chart for your application.

After the first run you will receive an **Access Token**.
> **⚠️ WARNING:** Copy the value of the access token and add it in the **values.yaml** file in the **secret section**

```bash
  AMQP_HOST_STRING: "amqp://admin:mypass@rabbitmq:5672/"
  DB_PASS: "postgres"
  RABBITMQ_DEFAULT_PASS: "mypass"
  REDIS_PASSWORD: "11110000"
  ACCESS_TOKEN: "fefjiehnyr"
```



## How to update Auditor
To update the Auditor to the latest version, follow these steps:
## Using the containers:
### Update using GitLab CI (Ansible playbook)

For the update it is enough to run pipeline and the script will autonomously update Auditor to the latest version 
![Image](docs/images/ci_deploy.jpg)


### Manual update
1. Stop the application:

```bash
docker-compose down -v
```
This will stop all services and remove the associated volumes, which will clean up the environment.
2. Pull the latest changes from the repository:

```bash
git pull
```

This ensures that you have the most up-to-date codebase to work with.

3. Restart the application:

```bash
docker-compose up -d
```
## Update in Kubernetes environment:
1. To update, run the following command:
```bash
helm upgrade auditor <path-to-helm-directory>
```
replace *<path-to-helm-directory>* with the path to the directory that contains the Helm Chart for your application.



